import request from '@/utils/request'

export function getArticlesByTag(paramMap) {
  return request({
    url: '/tag/' + paramMap.name,
    method: 'get',
    params: {
      pageNo: paramMap.pageNo,
      pageSize: paramMap.pageSize
    }
  })
}
